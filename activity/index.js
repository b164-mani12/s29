const express = require('express');

const app = express();

const port = 4000;

app.use(express.json());

app.use(express.urlencoded({ extended:true }));

let users = [];

//welcome page
app.get("/home", (req, res) => {
	res.send("Hello! Have a nice day!")
});

//users list
app.get("/users", (req, res) => {
	res.send(users);

})

//add users
app.post("/signup", (req, res) => {
	console.log(req.body);
	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send("Please input BOTH username and password")
	}
})

//delete users
app.delete("/delete", (req, res) => {

let message;

	for(let i = 0; i < users.length; i++){
		//if the username provided in the postman/client and the username of the current object in the loop is the same.
		if(req.body.username === users[i].username){
			//change the password of the user found by the loop into the provided in the client
			users.splice(users[i], 1);

			//message response 
			message = `User ${req.body.username} has been deleted`;

			//Breaks out of the loop once a user matches the username provided.
			break;
		}else {
			message = "User does not exist"
		}
	}

	res.send(message);

})

app.listen(port, ()=> console.log(`Server running at port:${port}`));


